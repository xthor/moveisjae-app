/*global LocalFileSystem,FileTransfer,FileReader*/
/*jslint devel:true*/
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var home_url = 'http://www.moveisjae.com.br',
    storage = 'AmoveisJae',
    pageLog = function (text) {
        'use strict';
        var body = document.getElementsByTagName('body')[0],
            pElem = document.createElement('p');
        if (typeof text === 'object') {
            text = JSON.stringify(text);
        }
        pElem.textContent = text;
        body.appendChild(pElem);
    },
    insertImage = function (image) {
        'use strict';
        var body = document.getElementsByTagName('body')[0],
            imgElem = document.createElement('img');
        imgElem.setAttribute('width', 312);
        imgElem.setAttribute('height', 281);
        imgElem.setAttribute('src', image);
        body.appendChild(imgElem);
    },
    readDataUrl = function (file) {
        'use strict';
        var reader = new FileReader();
        reader.onloadend = function (evt) {
            console.log('Read as data URL');
            console.log(evt.target.result);
            insertImage(evt.target.result);
        };
        reader.readAsDataURL(file);
    },
    gotFile = function (file) {
        'use strict';
        readDataUrl(file);
    },
    fail = function (evt) {
        'use strict';
        console.log(evt.target.error.code);
    },
    gotFileEntry = function (fileEntry) {
        'use strict';
        fileEntry.file(gotFile, fail);
    },
    gotFS = function (fileSystem) {
        'use strict';
        fileSystem.root.getFile('placeholder.jpg', {create: true}, gotFileEntry, fail);
    },
    onFail = function (message) {
        'use strict';
        alert('Failed because: ' + message);
    },
    app = {
        // Application Constructor
        initialize: function () {
            'use strict';
            this.bindEvents();
        },
        // Bind Event Listeners
        //
        // Bind any events that are required on startup. Common events are:
        // 'load', 'deviceready', 'offline', and 'online'.
        bindEvents: function () {
            'use strict';
            document.addEventListener('deviceready', this.onDeviceReady, false);
        },
        // deviceready Event Handler
        //
        // The scope of 'this' is the event. In order to call the 'receivedEvent'
        // function, we must explicitly call 'app.receivedEvent(...);'
        onDeviceReady: function () {
            'use strict';
            app.receivedEvent('deviceready');
        },

        pageLog: function (text) {
            'use strict';
            pageLog(text);
        },

        createDirectory: function (name) {
            'use strict';
            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
                fs.root.getDirectory(name, { create: true });
            });
        },

        transferUpload: function (subdir) {
            'use strict';
            var fileTransfer = new FileTransfer(),
                uri = home_url + '/wp-content/uploads/' + subdir,
                file;
            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
                file = fs.root.toURL() + storage + '/uploads/' + subdir;
                fileTransfer.download(
                    encodeURI(uri),
                    file,
                    function (entry) {
                        // console.log('Success with #getFile method. [transferUpload]');
                        // console.log(entry);
                        app.pageLog('Success with #getFile method. [transferUpload]');
                        app.insertImage(entry.nativeURL);
                        app.pageLog(JSON.stringify(entry));
                    },
                    function (error) {
                        // console.log('Error with #getFile method. [transferUpload]', error);
                        // console.log('Error with #download method.', error);
                        app.pageLog('Error with #getFile method. [transferUpload]');
                    }
                );
            });
        },

        downloadUpload: function (subdir) {
            'use strict';
            var file;
            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
                file = /*fs.root.fullPath + */storage + '/uploads/' + subdir;
                app.pageLog('file: ' + file);
                fs.root.getFile(
                    file,
                    {create: false},
                    function (entry) {
                        // console.log('Success with #download method. [downloadUpload]');
//                         console.log(entry);
                        app.pageLog('Success with #getfile method. [downloadUpload]');
                    },
                    function (error) {
                        // console.log('Error with #download method. [downloadUpload]', error);
                        app.transferUpload(subdir);
//                        app.pageLog(file);
                        app.pageLog('Error with #getfile method. [downloadUpload]');
                    }
                );
            });
        },

        downloadUploads: function () {
            'use strict';
            app.createDirectory('AmoveisJae');
            var images = [
                '2015/04/Asturia-80-C-Pes-Vicenza-Amarelo-Retro-Gavetao-312x281.jpg',
                '2015/04/Asturia-65-C-Pes-Vicenza-Aspen-Portas-e-gavetas-312x281.jpg'
            ];
            images.forEach(function (image) {
                app.downloadUpload(image);
            });
            console.log('Finish Downloads');

            app.insertImages();
        },

        insertImages: function () {
            'use strict';
            console.log('Inserting images');
            var images = [
                    '2015/04/Asturia-80-C-Pes-Vicenza-Amarelo-Retro-Gavetao-312x281.jpg',
                    '2015/04/Asturia-65-C-Pes-Vicenza-Aspen-Portas-e-gavetas-312x281.jpg'
                ],
                body = document.getElementsByTagName('body')[0],
                src;
            images.forEach(function (image) {
                window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
                    image = /*fs.root.fullPath + */storage  + '/uploads/' + image;
//                    app.pageLog(JSON.stringify(fs.root));
//                    image = fs.root.fullPath + storage  + '/' + image;
                    app.pageLog('image: ' + image);
                    fs.root.getFile(
                        image,
                        {create: false},//{create: true, exclusive: false},
                        function (entry) {
                            console.log('Success with #getFile method. [insertImages]');
                            // console.log(entry);
                            pageLog(entry);
                            var reader = new FileReader();
                            reader.onloadend = function (event) {
                                pageLog(event);
                                src = event.target.result;
                                console.log('Read as data URL');
                                console.log(src);
                                insertImage(src);
                            };
                            reader.readAsDataURL(image);

                            app.pageLog('Success with #getFile method. [insertImages]');
                        },
                        function (error) {
                            // console.log('Error with #getFile method. [insertImages]', error);
                            app.pageLog('Error with #getFile method. [insertImages]');
                        }
                    );
                });
            });
        },

        imageSize: function (imageURI) {
            'use strict';
            // This function is called once an imageURI is rerturned from PhoneGap's camera or gallery function
            window.resolveLocalFileSystemURL(
                imageURI,
                function (fileEntry) {
                    console.log(imageURI);
                    app.pageLog(imageURI);
                    fileEntry.file(function (fileObject) {
                        // Create a reader to read the file
                        var reader = new FileReader();

                        // Create a function to process the file once it's read
                        reader.onloadend = function (evt) {
                            console.log(imageURI);
                            app.pageLog(imageURI);
                            // Create an image element that we will load the data into
                            var image = new Image(),
                                image_width,
                                image_height;
                            image.onload = function (evt) {
                                // The image has been loaded and the data is ready
                                image_width = this.width;
                                image_height = this.height;
                                app.pageLog('IMAGE HEIGHT: ' + image_height);
                                console.log('IMAGE HEIGHT: ' + image_height);
                                console.log('IMAGE WIDTH: ' + image_width);
                                app.pageLog('IMAGE WIDTH: ' + image_width);
                                // We don't need the image element anymore. Get rid of it.
                                image = null;
                            };
                            // Load the read data into the image source. It's base64 data
                            image.src = evt.target.result;
                        };
                        // Read from disk the data as base64
                        reader.readAsDataURL(fileObject);
                    },
                        function () {
                            console.log('There was an error reading or processing this file.');
                            app.pageLog('There was an error reading or processing this file.');
                        }
                                  );
                }
            );
        },

        // Update DOM on a Received Event
        receivedEvent: function (id) {
            'use strict';
            var parentElement = document.getElementById(id),
                listeningElement = parentElement.querySelector('.listening'),
                receivedElement = parentElement.querySelector('.received'),
                body = document.getElementsByTagName('body')[0],
                image,
                images = [
                    '2015/04/Asturia-80-C-Pes-Vicenza-Amarelo-Retro-Gavetao-312x281.jpg',
                    '2015/04/Asturia-65-C-Pes-Vicenza-Aspen-Portas-e-gavetas-312x281.jpg'
                ];

            listeningElement.classList.add('hidden');
            receivedElement.classList.remove('hidden');

            console.log('Received Event: ' + id);

            body.innerHTML = '';

//            images.forEach(function (image) {
//                window.requestFileSystem(
//                    LocalFileSystem.PERSISTENT,
//                    0,
//                    function (fs) {
//                        image = fs.root.fullPath + storage  + '/uploads/' + image;
//                        console.log(image);
//                        app.pageLog(image);
//                        app.imageSize(image);
//                    }
//                );
//            });
            app.downloadUploads();
//            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, onFail);
        }
    };
