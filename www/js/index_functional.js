/*global LocalFileSystem,FileTransfer,FileReader,Event*/
/*jslint devel:true*/
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
if (!Array.prototype.last) {
    Array.prototype.last = function () {
        'use strict';
        return this[this.length - 1];
    };
}
var home_url = 'http://www.moveisjae.com.br',
    storage = 'MoveisJae',
    images = [
        '2015/04/Asturia-80-C-Pes-Vicenza-Amarelo-Retro-Gavetao-312x281.jpg',
        '2015/04/Asturia-65-C-Pes-Vicenza-Aspen-Portas-e-gavetas-312x281.jpg'
    ],
    pageLog = function (text) {
        'use strict';
        var body = document.getElementsByTagName('body')[0],
            pElem = document.createElement('p');
        if (typeof text === 'object') {
            text = JSON.stringify(text);
        }
        pElem.textContent = text;
        body.appendChild(pElem);
    },
    insertImage = function (image) {
        'use strict';
        var body = document.getElementsByTagName('body')[0],
            imgElem = document.createElement('img');
        imgElem.setAttribute('width', 312);
        imgElem.setAttribute('height', 281);
        imgElem.setAttribute('src', image);
        body.appendChild(imgElem);
    },
    app = {
        // Application Constructor
        initialize: function () {
            'use strict';
            this.bindEvents();
        },
        // Bind Event Listeners
        //
        // Bind any events that are required on startup. Common events are:
        // 'load', 'deviceready', 'offline', and 'online'.
        bindEvents: function () {
            'use strict';
            document.addEventListener('deviceready', this.onDeviceReady, false);
            window.addEventListener('offline', this.switchConnectState, false);
            window.addEventListener('online', this.switchConnectState, false);
        },
        // deviceready Event Handler
        //
        // The scope of 'this' is the event. In order to call the 'receivedEvent'
        // function, we must explicitly call 'app.receivedEvent(...);'
        onDeviceReady: function () {
            'use strict';
            app.receivedEvent('deviceready');
        },

        switchConnectState: function (e) {
            'use strict';
            var body = document.getElementsByTagName('body')[0];

            if (navigator.onLine) {
                body.classList.add('online');
                body.classList.remove('offline');
                // console.log('Device is online');
            } else {
                body.classList.add('offline');
                body.classList.remove('online');
                // console.log('Device is offline');
            }
        },

        createDirectory: function (name) {
            'use strict';
            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
                fs.root.getDirectory(name, { create: true });
            });
        },

        transferUpload: function (image) {
            'use strict';
            var fileTransfer = new FileTransfer(),
                uri = home_url + '/wp-content/uploads/' + image,
                file;
            window.requestFileSystem(
                LocalFileSystem.PERSISTENT,
                0,
                function (fs) {
                    file = fs.root.toURL() + storage + '/uploads/' + image;
                    fileTransfer.download(
                        encodeURI(uri),
                        file,
                        function (entry) {
                            // console.log('Success with #fileTransfer method. [transferUpload]');
                            // console.log('image');
                            // console.log(image);
                            // console.log('last image in array images');
                            // console.log(images.last());
                            if (image === images.last()) {
                                //console.log('Last');
                                app.insertImages();
                            } /*else {
                                console.log('No last');
                            }*/
                        },
                        function (error) {
                            // console.log('Error with #fileTransfer method. [transferUpload]', error);
                            // console.log('image');
                            // console.log(image);
                            // console.log('last image in array images');
                            // console.log(images.last());
                            if (image === images.last()) {
                                // console.log('Last');
                                app.insertImages();
                            } /*else {
                                console.log('No last');
                            }*/
                        }
                    );
                }
            );


        },

        downloadUploads: function () {
            'use strict';
            console.log('Start Downloads');
            app.createDirectory(storage);
            var image,
                callback;
            images.forEach(function (image) {
//                console.log('current image');
//                console.log(image);
//                console.log('last image in array');
//                console.log(images.last());
                window.requestFileSystem(
                    LocalFileSystem.PERSISTENT,
                    0,
                    function (fs) {
                        fs.root.getFile(
                            storage + '/uploads/' + image,
                            {create: false},
                            function (entry) {
                                // console.log('Success with #download method. [downloadUpload]');
                                // console.log('image');
                                // console.log(image);
                                // console.log('last image in array images');
                                // console.log(images.last());
                                if (image === images.last()) {
                                    // console.log('Last');
                                    app.insertImages();
                                } /*else {
                                    console.log('No last');
                                }*/
                            },
                            function (error) {
                                // console.log('Error with #download method. [downloadUpload]', error);
                                app.transferUpload(image);
                            }
                        );
                    }
                );
            });
//            console.log('Finish Downloads');
        },

        insertImages: function () {
            'use strict';
            console.log('Inserting images');
            var body = document.getElementsByTagName('body')[0];
            images.forEach(function (image) {
                window.requestFileSystem(
                    LocalFileSystem.PERSISTENT,
                    0,
                    function (fs) {
                        image = storage + '/uploads/' + image;
                        fs.root.getFile(
                            image,
                            {create: true},
                            function (entry) {
                                // console.log('Success with #getFile method. [insertImages]');
                                insertImage(entry.toURL());
                            },
                            function (evt) {
                                // console.log('Error with #getFile method. [insertImages]');
                                // console.log(evt.target.error.code);
                            }
                        );
                    },
                    function (message) {
                        console.log('Failed because: ' + message);
                    }
                );
            });
        },

        // Update DOM on a Received Event
        receivedEvent: function (id) {
            'use strict';
            var parentElement = document.getElementById(id),
                listeningElement = parentElement.querySelector('.listening'),
                receivedElement = parentElement.querySelector('.received'),
                body = document.getElementsByTagName('body')[0];

            listeningElement.classList.add('hidden');
            receivedElement.classList.remove('hidden');

            console.log('Received Event: ' + id);

            body.innerHTML = '';

            app.downloadUploads();
        }
    };
