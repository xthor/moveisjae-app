/*global LocalFileSystem,FileTransfer,FileReader,Event,$*/
/*jslint devel:true*/
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
if (!Array.prototype.last) {
    Array.prototype.last = function () {
        'use strict';
        return this[this.length - 1];
    };
}
//if (!Object.prototype.toKeysValues) {
//    Object.prototype.toKeysValues = function () {
//        'use strict';
//        var obj = this,
//            arr;
//        console.log(obj);
//        arr = Object.keys(obj).map(function (key) {
//            return obj[key];
//        });
//        return arr;
//    };
//}
function isNotNull(value) {
    'use strict';
    return value !== null;
}
var home_url = 'http://www.moveisjae.com.br',
    permastruct,
    extractSlug = function (str) {
        'use strict';
        var re = /([\w\d\-]+)\/$/,
            m;
        if ((m = re.exec(str)) !== null) {
            if (m.index === re.lastIndex) {
                re.lastIndex += 1;
            }
            return m[0];
        }
    },
    productSlug = function (id) {
        'use strict';
        var products = JSON.parse(localStorage.getItem('products'));
        return products[id];
    },
    storage = 'MoveisJae',
    placeholder_img = 'img/placeholder.jpg',
    images = [],
    pageLog = function (text) {
        'use strict';
        var body = document.getElementsByTagName('body')[0],
            pElem = document.createElement('p');
        if (typeof text === 'object') {
            text = JSON.stringify(text);
        }
        pElem.textContent = text;
        body.appendChild(pElem);
    },
    insertImage = function (image) {
        'use strict';
        var body = document.getElementsByTagName('body')[0],
            imgElem = document.createElement('img');
        imgElem.setAttribute('width', 312);
        imgElem.setAttribute('height', 281);
        imgElem.setAttribute('src', image);
        body.appendChild(imgElem);
    },
    app = {
        // Application Constructor
        initialize: function () {
            'use strict';
            this.bindEvents();
        },
        // Bind Event Listeners
        //
        // Bind any events that are required on startup. Common events are:
        // 'load', 'deviceready', 'offline', and 'online'.
        bindEvents: function () {
            'use strict';
            document.addEventListener('deviceready', this.onDeviceReady, false);
            window.addEventListener('offline', this.switchConnectState, false);
            window.addEventListener('online', this.switchConnectState, false);
        },
        // deviceready Event Handler
        //
        // The scope of 'this' is the event. In order to call the 'receivedEvent'
        // function, we must explicitly call 'app.receivedEvent(...);'
        onDeviceReady: function () {
            'use strict';
            app.receivedEvent('deviceready');
            app.init();
        },

        createDirectory: function (name) {
            'use strict';
            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
                fs.root.getDirectory(name, { create: true });
            });
        },

        transferUpload: function (image) {
            'use strict';
            var fileTransfer = new FileTransfer(),
                uri = home_url + '/wp-content/uploads/' + image,
                file;
            window.requestFileSystem(
                LocalFileSystem.PERSISTENT,
                0,
                function (fs) {
                    file = fs.root.toURL() + storage + '/uploads/' + image;
                    fileTransfer.download(
                        encodeURI(uri),
                        file,
                        function (entry) {
                            if (image === images.last()) {
                                // app.insertImages();
                                console.log('Finish downloads');
                            }
                        },
                        function (error) {
                            if (image === images.last()) {
                                // app.insertImages();
                                console.log('Finish downloads');
                            }
                        }
                    );
                }
            );


        },

        downloadUploads: function () {
            'use strict';
            console.log('Start Downloads');
            app.createDirectory(storage);
            var image,
                callback,
                data = app.getData();

            data.products = Object.keys(data.products).map(function (key) {
                data.products[key].slug = key;
                return data.products[key];
            });

            data.products.forEach(function (item) {
                images.push(item.featured_src);
                if (item.id === 3191) {
                    Object.keys(item.variations).map(function (term) {
                        images.push(item.variations[term].image);
                        images.push(item.variations[term].sample);
                    });
                }
            });

            images.forEach(function (image) {
                window.requestFileSystem(
                    LocalFileSystem.PERSISTENT,
                    0,
                    function (fs) {
                        fs.root.getFile(
                            storage + '/uploads/' + image,
                            {create: false},
                            function (entry) {
                                if (image === images.last()) {
                                    // app.insertImages();
                                    console.log('Finish uploads');
                                }
                            },
                            function (error) {
                                app.transferUpload(image);
                            }
                        );
                    }
                );
            });
        },

        insertImages: function () {
            'use strict';
            console.log('Inserting images');
            var body = document.getElementsByTagName('body')[0];
            images.forEach(function (image) {
                window.requestFileSystem(
                    LocalFileSystem.PERSISTENT,
                    0,
                    function (fs) {
                        image = storage + '/uploads/' + image;
                        fs.root.getFile(
                            image,
                            {create: true},
                            function (entry) {
                                insertImage(entry.toURL());
                            },
                            null
                        );
                    },
                    function (message) {
                        console.log('Failed because: ' + message);
                    }
                );
            });
        },

        init: function () {
            'use strict';

            window.scroll(0, 0);
            app.menuAbout();
            app.switchConnectState();
            app.cacheData();
        },

        // Update DOM on a Received Event
        receivedEvent: function (id) {
            'use strict';
            var parentElement = document.getElementById(id),
                listeningElement = parentElement.querySelector('.listening'),
                receivedElement = parentElement.querySelector('.received'),
                body = document.getElementsByTagName('body')[0];

            listeningElement.classList.add('hidden');
            receivedElement.classList.remove('hidden');

            console.log('Received Event: ' + id);

//            body.innerHTML = '';
//
//
        },

        switchConnectState: function (e) {
            'use strict';
            var body = document.getElementsByTagName('body')[0];

            if (navigator.onLine) {
                body.classList.add('online');
                body.classList.remove('offline');
            } else {
                body.classList.add('offline');
                body.classList.remove('online');
            }
        },

        fireEvent: function (name, data) {
            'use strict';
            var e = document.createEvent('Event');
            e.initEvent(name, true, true);
            e.data = data;
            window.dispatchEvent(e);
        },

        fetch: function (url, callback) {
            'use strict';
            callback = typeof callback !== 'undefined' ? callback : app.dataLoaded;
            var xhr = new XMLHttpRequest(),

                maxWaitTime = 20000,

                noResponseTimer = setTimeout(function () {
                    xhr.abort();
                    app.fireEvent('connectiontimeout', {});
                    if (!!localStorage[url]) {
                        // We have some data cached, return that to the callback.
                        callback(localStorage[url]);
                        return;
                    }
                }, maxWaitTime);

            xhr.onreadystatechange = function (e) {

                if (xhr.readyState !== 4) {
                    return;
                }

                if (xhr.status === 200) {
                    app.fireEvent('goodconnection', {});
                    clearTimeout(noResponseTimer);
                    // Save the data to local storage
                    localStorage[url] = xhr.responseText;
                    // call the handler
                    callback(xhr.responseText);
                } else {
                    app.fireEvent('connectionerror', {});
                    // There is an error of some kind, use our cached copy (if available).
                    if (!!localStorage[url]) {
                        // We have some data cached, return that to the callback.
                        callback(localStorage[url]);
                        return;
                    }
                }
            };
            xhr.open('GET', url);
            xhr.send();
        },

        cacheData: function () {
            'use strict';
            app.fetch('http://www.moveisjae.com.br/offline', app.dataLoaded);
        },

        getData: function () {
            'use strict';
            var data = localStorage.getItem('data');
            data = JSON.parse(data);
            return data;
        },

        appParams: function () {
            'use strict';
            var data = app.getData();
            home_url = data.home_url;
            permastruct = data.permastruct;
            images = Object.keys(data.products).map(function (key) {
                return data.products[key].featured_src;
            });
        },

        retrieveProductCategories: function () {
            'use strict';
            var data,
                range = {};

            data = app.getData();

            range.top = [];
            range.mdp = [];

            data.categories = Object.keys(data.categories).map(function (key) {
                data.categories[key].slug = key;
                return data.categories[key];
            });

            data.categories.forEach(function (term) {
                delete term.products;
                if (term.taxonomy === 'linhamdp') {
                    range.mdp.push(term);
                } else {
                    range.top.push(term);
                }
            });

            localStorage.setItem('categories', JSON.stringify(range));
        },

        productSlugs: function () {
            'use strict';
            var data = app.getData(),
                products = {};

            data.products = Object.keys(data.products).map(function (key) {
                data.products[key].slug = key;
                return data.products[key];
            });

            data.products.forEach(function (item) {
                products[item.id] = item.slug;
            });
            localStorage.setItem('products', JSON.stringify(products));
        },

        dataLoaded: function (json) {
            'use strict';

            var body = document.getElementsByTagName('body')[0];
            window.scroll(0, 0);
            body.classList.remove('loading');

            localStorage.setItem('data', json);
            app.appParams();
            app.retrieveProductCategories();
            app.menuProductCats();
            app.productSlugs();
            app.downloadUploads();
        },

        menuAbout: function () {
            'use strict';
            app.menuAboutClick();
        },

        submenuProductCats: function (menuid, items) {
            'use strict';
            var submenu = $('#menu-item-' + menuid + ' .sub-menu'),
                taxonomy,
                itemid,
                menuitem,
                href;
            submenu.html('');
            items.forEach(function (term, menuorder) {
                itemid = 'menu-item-' + term.id;
                menuitem = $('<li>').attr('id', itemid).addClass(itemid);
                taxonomy = term.taxonomy;
                href = permastruct[taxonomy];
                href = href.replace('%%' + taxonomy + '%%', term.slug) + '/';
//                console.log(term);
                menuitem.prepend($('<a>').attr('href', href).text(term.name));
                submenu.append(menuitem);
            });
        },

        menuProductCats: function () {
            'use strict';
            var categories = JSON.parse(localStorage.getItem('categories'));
            app.submenuProductCats(298, categories.top.filter(isNotNull));
            app.submenuProductCats(1181, categories.mdp.filter(isNotNull));
            app.menuItemProductCatsClick();
            $('#menu-item-173, #menu-item-1193, #menu-item-172').click(function (e) {
                e.preventDefault();
            });
        },

        insertProductSample: function (variation) {
            'use strict';
            var imgElem = $('<img>'),
                item = $('<li>').addClass(variation.slug),
                circle = $('<span>');
//            imgElem.attr('width', '47');
//            imgElem.attr('height', '47');
//            imgElem.attr('src', sample);
//            imgElem.attr('alt', 'Amostra');
            if ($('li.' + variation.slug).length < 1) {/* && variation.slug !== ''*/
                if (variation.sample.slice(-3) !== 'bmp') {
                    circle.css('background-image', 'url(' + variation.sample + ')');
                }
                //item = $('li.' + variation.slug);
                item.html(circle);
//                console.log('Vsl: ' + variation.slug);
//                console.log('Vsa: ' + variation.sample);
                $('#collapseOne .panel-body ol').append(item);
            }
            //else {
//                item.html(circle);
//
//                $('#collapseOne .panel-body ol').append(item);
//            }
        },

        productVariationSample: function (variation) {
            'use strict';
            var sample = variation.sample;
            window.requestFileSystem(
                LocalFileSystem.PERSISTENT,
                0,
                function (fs) {
                    sample = storage + '/uploads/' + sample;
                    fs.root.getFile(
                        sample,
                        {create: true},
                        function (entry) {
                            variation.sample = entry.toURL();
                            app.insertProductSample(variation);
                        },
                        function (error) {
                            variation.sample = placeholder_img;
                            app.insertProductSample(variation);
                        }
                    );
                },
                function (message) {
                    console.log('Failed because: ' + message);
                }
            );
        },

        productVariation: function (variation) {
            'use strict';
            app.productVariationSample(variation);
//            var image;
////            var href,
////                image,
////                title,
////                productitem,
////                src;
////            productitem = $('<div>').addClass('col-sm-4 post-' + id + ' product type-product produto type-produto');
////            href = 'produto/' + slug + '/';
////            image = $('<img>');
////            image.attr('width', '312');
////            image.attr('height', '281');
//            delete product.term.products;
//
////                src = app.uploadImage(product.featured_src);
//            image = product.featured_src;
//            window.requestFileSystem(
//                LocalFileSystem.PERSISTENT,
//                0,
//                function (fs) {
//                    image = storage + '/uploads/' + image;
//                    fs.root.getFile(
//                        image,
//                        {create: true},
//                        function (entry) {
//                            product.featured_src = entry.toURL();
//                            //console.log(product.featured_src);
//                            app.insertProductListItem(product);
//                        },
//                        function (error) {
//                            product.featured_src = placeholder_img;
//                            //console.log(product.featured_src);
//                            app.insertProductListItem(product);
//                        }
//                    );
//                },
//                function (message) {
//                    console.log('Failed because: ' + message);
//                }
//            );
        },

        productVariations: function (variations) {
            'use strict';
//            console.log(JSON.stringify(variations));
//            var body = document.getElementsByTagName('body')[0],
//                data = app.getData(),
//                category = data.categories[slug],
//                product;
//
//            body.classList.remove('home');
//            body.classList.add('product-cat');
//            window.scroll(0, 0);
//
//            $('.navbar-collapse').collapse('toggle');
//
//            $('.tax-product_cat .page-title').text('Linha ' + category.name);
//            //$('.tax-product_cat .term-description p').text(category.description);
//
            var samplePanel = $('#collapseOne .panel-body'),
                descList = $('<dd>');
            samplePanel.html('');
            descList.append($('<dt>').text('Pintura BP'));
            descList.append($('<dd>').html($('<ol>').addClass('samples')));
            samplePanel.html(descList);
//
//            delete category.id;
//            delete category.name;
//            delete category.description;
            variations = Object.keys(variations).map(function (i) {
                //console.log('Color: ' + variations[i].slug);
                //app.productVariation(variations[i]);
                return variations[i];
            });

            variations.forEach(function (variation) {
                console.log('Color: ' + variation.slug);
                if (variation.slug !== null) {
                    app.productVariation(variation);
                }
            });
//
//            category.products.forEach(function (id) {
//                slug = productSlug(id);
//                product = data.products[slug];
//                product.slug = slug;
//                product.term = category;
//                app.productListItem(product);
//            });
        },

        insertProductViewItem: function (product) {
            'use strict';
            var body = document.getElementsByTagName('body')[0],
                imgElem = $('<img>'),
                leftCol = $('.product-single .img'),
                titleElem = $('.product-single .product-title');
            imgElem.attr('width', '550');
            imgElem.attr('height', '495');
            imgElem.attr('src',  product.featured_src);
            imgElem.attr('alt', product.title);

            leftCol.html(imgElem);
            titleElem.text(product.title);
            $('.product-single .description').html(product.description);
            app.productVariations(product.variations);
        },

        productViewItem: function (product) {
            'use strict';
            var image;
            image = product.featured_src;
            window.requestFileSystem(
                LocalFileSystem.PERSISTENT,
                0,
                function (fs) {
                    image = storage + '/uploads/' + image;
                    fs.root.getFile(
                        image,
                        {create: true},
                        function (entry) {
                            product.featured_src = entry.toURL();
                            //console.log(product.featured_src);
                            app.insertProductViewItem(product);
                        },
                        function (error) {
                            product.featured_src = placeholder_img;
                            //console.log(product.featured_src);
                            app.insertProductViewItem(product);
                        }
                    );
                },
                function (message) {
                    console.log('Failed because: ' + message);
                }
            );
        },

        viewProductSingle: function (slug) {
            'use strict';
            var body = document.getElementsByTagName('body')[0],
                data = app.getData(),
                product = data.products[slug];

            body.classList.remove('product-cat');
            body.classList.add('product-view');
            window.scroll(0, 0);
            app.productViewItem(product);
        },

        insertProductListItem: function (product) {
            'use strict';
            var body = document.getElementsByTagName('body')[0],
//                itemElem = document.createElement('div'),
                itemElem = $('<div>').addClass('col-sm-4 post-' + product.id + ' product type-product produto type-produto'),
                linkElem = document.createElement('a'),
//                imgElem = document.createElement('img'),
                imgElem = $('<img>'),
//                titleElem = document.createElement('h2'),
                titleElem = $('<h2>'),
                productsElem = document.querySelector('.tax-product_cat .products'),
                imgLinkElem,
                taxonomy,
                href,
                term,
                classes,
                slug;
            term = product.term;
            taxonomy = term.taxonomy;
            href = permastruct[taxonomy];
            href = href.replace('%%' + taxonomy + '%%', product.slug) + '/';
//            classes = [
//                'col-sm-4',
//                'post-' + product.id,
//                'product',
//                'type-product',
//                'produto',
//                'type-produto'
//            ];
//            classes.forEach(function (value) {
//                itemElem.classList.add(value);
//            });
//            imgElem.setAttribute('width', 312);
            imgElem.attr('width', '312');
//            imgElem.setAttribute('height', 281);
            imgElem.attr('height', '281');
//            imgElem.setAttribute('src', product.featured_src);
            imgElem.attr('src',  product.featured_src);
//            imgElem.setAttribute('alt', product.title);
            imgElem.attr('alt', product.title);

//            linkElem.setAttribute('href', href);

//            imgLinkElem = linkElem;
//            imgLinkElem.appendChild(imgElem);

//            titleElem.html($('<a>').text(product.title));

            itemElem.prepend($('<a>').attr('href', href).html(imgElem));
//            itemElem.append(titleElem);
            titleElem.html($('<a>').attr('href', href).text(product.title));
            itemElem.append(titleElem);

//            linkElem.textContent = product.title;
//            titleElem.appendChild(linkElem);
            $('.tax-product_cat .products').append(itemElem);
            $('.tax-product_cat a').click(function (e) {
                e.preventDefault();
                slug = $(this).attr('href');
                slug = extractSlug(slug).replace('/', '');
                app.viewProductSingle(slug);
            });

//            itemElem.appendChild(imgLinkElem);
//            itemElem.appendChild(titleElem);

//            productsElem.appendChild(itemElem);
        },

        productListItem: function (product) {
            'use strict';
            var image;
//            var href,
//                image,
//                title,
//                productitem,
//                src;
//            productitem = $('<div>').addClass('col-sm-4 post-' + id + ' product type-product produto type-produto');
//            href = 'produto/' + slug + '/';
//            image = $('<img>');
//            image.attr('width', '312');
//            image.attr('height', '281');
            delete product.term.products;

//                src = app.uploadImage(product.featured_src);
            image = product.featured_src;
            window.requestFileSystem(
                LocalFileSystem.PERSISTENT,
                0,
                function (fs) {
                    image = storage + '/uploads/' + image;
                    fs.root.getFile(
                        image,
                        {create: true},
                        function (entry) {
                            product.featured_src = entry.toURL();
                            //console.log(product.featured_src);
                            app.insertProductListItem(product);
                        },
                        function (error) {
                            product.featured_src = placeholder_img;
                            //console.log(product.featured_src);
                            app.insertProductListItem(product);
                        }
                    );
                },
                function (message) {
                    console.log('Failed because: ' + message);
                }
            );
            //        console.log('SRC: ' + src);
//                image.attr('src', src);
//                image.attr('alt', product.title);

//            title = $('<h2>');
//            title.html($('<a>').text(product.title));
////                productitem.prepend($('<a>').attr('href', href).html(image));
//            productitem.append(title);
//            $('.tax-product_cat .products').append(productitem);
//            $('.tax-product_cat a').click(function (e) {
//                e.preventDefault();
//            });
        },

        switchAbout: function () {
            'use strict';
            var body = document.getElementsByTagName('body')[0];

            body.classList.remove('home');
            body.classList.remove('product-cat');
            body.classList.remove('product-view');
            body.classList.add('about-page');
            $('.navbar-collapse').collapse('toggle');

            window.scroll(0, 0);
        },

        switchProductCat: function (slug) {
            'use strict';
            var body = document.getElementsByTagName('body')[0],
                data = app.getData(),
                category = data.categories[slug],
                product;

            body.classList.remove('home');
            body.classList.remove('about-page');
            body.classList.remove('product-view');
            body.classList.add('product-cat');
            window.scroll(0, 0);

            $('.navbar-collapse').collapse('toggle');

            $('.tax-product_cat .page-title').text('Linha ' + category.name);
            //$('.tax-product_cat .term-description p').text(category.description);

            $('.tax-product_cat .products').html('');

            delete category.id;
            delete category.name;
            delete category.description;

            category.products.forEach(function (id) {
                slug = productSlug(id);
                product = data.products[slug];
                product.slug = slug;
                product.term = category;
                app.productListItem(product);
            });
        },

        menuAboutClick: function () {
            'use strict';
            $('#menu-item-173 a').click(function (e) {
                e.preventDefault();
                app.switchAbout();
            });
        },

        menuItemProductCatsClick: function () {
            'use strict';
            var slug;
            $('#menu-item-298, #menu-item-1181').each(function () {
                $(this).find('.sub-menu a').each(function () {
                    $(this).click(function (e) {
                        e.preventDefault();
                        slug = $(this).attr('href');
                        slug = extractSlug(slug).replace('/', '');
                        app.switchProductCat(slug);
                    });
                });
            });
        }
    };
