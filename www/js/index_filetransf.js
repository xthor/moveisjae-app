/*global device,LocalFileSystem,FileTransfer,FileReader*/
/*jslint devel:true*/
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
//var showLink = function (url) {
//    'use strict';
//    alert(url);
//    var divEl = document.getElementById("ready"),
//      aElem = document.createElement("a");
//    aElem.setAttribute("target", "_blank");
//    aElem.setAttribute("href", url);
//    aElem.appendChild(document.createTextNode("Ready! Click To Open."));
//    divEl.appendChild(aElem);
//
//  },
//  fail = function (evt) {
//    'use strict';
//    console.log(evt.target.error.code);
//  },
//  downloadFile = function () {
//    'use strict';
//    window.requestFileSystem(
//      LocalFileSystem.PERSISTENT,
//      0,
//      function onFileSystemSuccess(fileSystem) {
//        fileSystem.root.getFile(
//          "dummy.html",
//          {create: true, exclusive: false},
//          function gotFileEntry(fileEntry) {
//            var sPath = fileEntry.fullPath.replace("dummy.html", ""),
//              fileTransfer = new FileTransfer();
//            fileEntry.remove();
//
//            fileTransfer.download(
//              "http://www.w3.org/2011/web-apps-ws/papers/Nitobi.pdf",
//              sPath + "theFile.pdf",
//              function (theFile) {
//                console.log("download complete: " + theFile.toURI());
//                showLink(theFile.toURI());
//              },
//              function (error) {
//                console.log("download error source " + error.source);
//                console.log("download error target " + error.target);
//                console.log("upload error code: " + error.code);
//              }
//            );
//          },
//          fail
//        );
//      },
//      fail
//    );
//  },
//var cacheFolderSubPath,
//  appCachePath,
//  filesFolderSubPath,
//  appFilesPath,
//  fail = function (evt) {
//    'use strict';
//    console.log(evt.target.error.code);
//  },
//  continueCustomExecution = function () {
//    'use strict';
//    // !! Assumes variable fileURL contains a valid URL to a path on the device,
//    //    for example, cdvfile://localhost/persistent/path/to/downloads/
//    console.log(fileURL);
//    var fileURL = appFilesPath,
//      fileTransfer = new FileTransfer(),
//      uri = encodeURI("http://www.moveisjae.com.br/wp-content/uploads/2015/04/Asturia-80-C-Pes-Vicenza-Amarelo-Retro-Gavetao-312x281.jpg");
//
//    fileTransfer.download(
//      uri,
//      fileURL,
//      function (entry) {
//        console.log("download complete: " + entry.toURL());
//      },
//      function (error) {
//        console.log("download error source " + error.source);
//        console.log("download error target " + error.target);
//        console.log("download error code" + error.code);
//      },
//      false,
//      {
//        headers: {
//          "Authorization": "Basic dGVzdHVzZXJuYW1lOnRlc3RwYXNzd29yZA=="
//        }
//      }
//    );
//  },
//  onResolveSuccess = function (fileEntry) {
//    'use strict';
//    appCachePath = fileEntry.toURL(); // file:///storage/sdcard0/Android/data/com.id.myApp/cache/ in my case but some other Androids have storage/emulated/0 or something like that ...
//    appFilesPath = fileEntry.toURL(); // file:///storage/sdcard0/Android/data/com.id.myApp/cache/ in my case but some other Androids have storage/emulated/0 or something like that ...
//    console.log(appFilesPath);
//    continueCustomExecution();
//  },
//  onResolveFail = function (err) {
//    'use strict';
//    console.log(err);
//  },
//  gotFS = function (fileSystem) {
//    'use strict';
//    var nturl = fileSystem.root.toURL(); // cdvfile://localhost/persistent/
////    console.log(nturl);
//    window.resolveLocalFileSystemURL(nturl + cacheFolderSubPath, onResolveSuccess, onResolveFail);
//    window.resolveLocalFileSystemURL(nturl + filesFolderSubPath, onResolveSuccess, onResolveFail);
//  },
var imageDownloader = {
    updateImages: function () {
      'use strict';
      this.remoteImages = [
        'http://www.moveisjae.com.br/wp-content/uploads/2015/04/Asturia-80-C-Pes-Vicenza-Amarelo-Retro-Gavetao-312x281.jpg',
        'http://www.moveisjae.com.br/wp-content/uploads/2015/04/Asturia-65-C-Pes-Vicenza-Aspen-Portas-e-gavetas-312x281.jpg'
      ];
      // ...code to get all of the images we need to download...
      // in my app, it's a `SELECT DISTINCT(image) FROM products;` query
      // example: [http://www.example.com/image1.jpg, http://www.example.com/image2.jpg, http://www.example.com/image3.jpg]
      this.downloadImages();
    },

    downloadImages: function () {
      'use strict';
      var object = this, // for use in the callbacks
        image,
        imageName;

      // stop if we've processed all of the images
      if (this.remoteImages.length === 0) {
        return;
      }

      // get the next image from the array
      image = this.remoteImages.pop();
      imageName = image.split('/').pop(); // just the image name (eg image1.jpg)
//      console.log('image: ' + image);
//      console.log('imageName: ' + imageName);

      window.requestFileSystem  = window.requestFileSystem || window.webkitRequestFileSystem;

      // access the filesystem
      // "LocalFileSystem.PERSISTENT" means it is permanent and not temporary
      window.requestFileSystem(
        LocalFileSystem.PERSISTENT,
        0,
        function (fileSystem) {
          var appFilesPath = cordova.file.externalDataDirectory,
            imageFile = appFilesPath + imageName;
          console.log('imageFile: ' + imageFile);
          // the "create: true" option will create the file if it doesn't exist
          // the "exclusive: true" option will skip the file if it already exists instead of re-downloading it
          fileSystem.root.getFile(
            imageFile,
            {create: true, exclusive: true},
            function (fileEntry) {
              // get the full path to the newly created file on the device
              var localPath = fileEntry.fullPath,
                remoteFile,
                fileTransfer;
              console.log('localPath: ' + localPath);

              // download the remote file and save it
              remoteFile = encodeURI(image);
              fileTransfer = new FileTransfer();
              console.log('remoteFile: ' + remoteFile);
              fileTransfer.download(
                remoteFile,
                localPath,
                function (newFileEntry) {
                  // successful download, continue to the next image
                  object.downloadImages();
                },
                function (error) { // error callback for #download
                  console.log('Error with #download method.', error);
                  object.downloadImages(); // continue working
                }
              );
            },
            function (error) { // error callback for #getFile
              console.log('Error with #getFile method.', error);
              object.downloadImages(); // continue working
            }
          );
        },
        function (error) { // error callback for #requestFileSystem
          console.log('Error with #requestFileSystem method.', error);
        }
      );
    }
  },
  app = {
    // Application Constructor
    initialize: function () {
      'use strict';
      this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function () {
      'use strict';
      document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    insertImage: function () {
      'use strict';
      var body = document.getElementsByTagName('body')[0],
        imgElem = document.createElement("img"),
        imageName = "Asturia-80-C-Pes-Vicenza-Amarelo-Retro-Gavetao-312x281.jpg";
      imgElem.setAttribute("width", 312);
      imgElem.setAttribute("height", 281);
      window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
        fileSystem.root.getFile(imageName, {create: false}, function (fileEntry) {
          var reader = new FileReader();
          reader.onloadend = function (event) { // callback for when file is read
            imgElem.setAttribute("src", event.target.result);
          };
          fileEntry.file(function (file) {
            reader.readAsDataURL(file);
          }, null);
        }, null);
      }, null);
      body.appendChild(imgElem);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function () {
      'use strict';
      app.receivedEvent('deviceready');
//      // do your thing!
//      downloadFile();
//      console.log('Device Platform: ' + device.platform);  // returns 'Android' or 'iOS' for instance
//      window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);
//      cacheFolderSubPath = "Android/data/br.com.moveisjae.app/cache/"; // this is app's cache folder that is removed when you delete your app! (I don't know what this path should be for iOS devices)
//      filesFolderSubPath = "Android/data/br.com.moveisjae.app/files/"; // this is app's cache folder that is removed when you delete your app! (I don't know what this path should be for iOS devices)
    },
    // Update DOM on a Received Event
    receivedEvent: function (id) {
      'use strict';
      var parentElement = document.getElementById(id),
        listeningElement = parentElement.querySelector('.listening'),
        receivedElement = parentElement.querySelector('.received');

      listeningElement.setAttribute('style', 'display:none;');
      receivedElement.setAttribute('style', 'display:block;');

      console.log('Received Event: ' + id);

      imageDownloader.updateImages();

//      app.insertImage();
    }
  };
