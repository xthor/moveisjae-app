/*global $, document, NetworkStatus, LocalFileSystem, FileTransfer, cordova*/
/*jslint devel: true, indent: 2*/
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
function isNotNull(value) {
  'use strict';
  return value !== null;
}

var online,
  home_url,
  permastruct,
  extractSlug = function (str) {
    'use strict';
    var re = /([\w\d\-]+)\/$/,
      m;
    if ((m = re.exec(str)) !== null) {
      if (m.index === re.lastIndex) {
        re.lastIndex += 1;
      }
      return m[0];
    }
  },
  extractUploadFile = function (str) {
    'use strict';
    var re = /uploads\/\d{4}\/\d{2}\/[\w\d\.\-]+$/,
      m;
    if ((m = re.exec(str)) !== null) {
      if (m.index === re.lastIndex) {
        re.lastIndex += 1;
      }
      re = /(\-\d+x\d+)(\.\w+)/;
      str = m[0];
      return str.replace(re, '$2');
    }
  },
  productSlug = function (id) {
    'use strict';
    var products = JSON.parse(localStorage.getItem('products'));
    return products[id];
  },
//  fail,
//  filesystem,
  // The directory to store data
  storage,
  placeholder_img = 'img/placeholder.jpg',
  resolved,
  fullpath,
  app = {
    // Application Constructor
    initialize: function () {
      'use strict';
      this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function () {
      'use strict';
      document.addEventListener('deviceready', this.onDeviceReady, false);
      window.addEventListener('offline', function (e) {
        app.switchConnectState();
      }, false);

      window.addEventListener('online', function (e) {
        app.switchConnectState();
      }, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function () {
      'use strict';
      app.receivedEvent();
      app.init();
    },
    downloadAsset: function (assetURL, fileName) {
      'use strict';
      resolved = false;
      if (typeof FileTransfer !== 'undefined') {
//        // we need to access LocalFileSystem
//        window.requestFileSystem(window.LocalFileSystem.PERSISTENT, 0, function(fs) {
//          // create the download directory is doesn't exist
//          fs.root.getDirectory('downloads', { create: true });
//
//          // we will save file in .. downloads/phonegap-logo.png
//          var filePath = fs.root.fullPath + '/downloads/' + url.split('/').pop(),
//            fileTransfer = new window.FileTransfer(),
//            uri = encodeURI(decodeURIComponent(assetURL));
//
//            fileTransfer.download(
//              uri,
//              filePath,
//              function (entry) {
//                console.log('Successfully downloaded file, full path is ' + entry.fullPath);
//              },
//              function(error) {
//                console.log('Some error ' + error.code + ' for ' + url);
//              },
//              false
//            );
//          }


//        console.log('storage ' + storage);
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
          // create the download directory is doesn't exist
//          fs.root.getDirectory('downloads', { create: true });
          //filePath = fs.root.fullPath + '/downloads/' + url.split('/').pop(),
//          fs.root.getDirectory('br.com.moveisjae.app', { create: true });
          var fileTransfer = new FileTransfer(),
            file;
          storage = fs.root.fullPath;
          storage = fs.root.toURL();
//          storage += 'br.com.moveisjae.app/';
//          console.log('storage ' + storage);
//          storage = fs.root.toURL();
          file = storage + fileName;
//          storage += 'Android/data/br.com.moveisjae.app/files/'
//          console.log('storage ' + storage);
//          console.log('file ' + file);
          fileTransfer.download(
            assetURL,

            // The correct path!
            file,

            function (entry) {
//              console.log('Success');
              resolved = true;
            },
            function (error) {
              console.log('download error source ' + error.source);
              console.log('download error target ' + error.target);
              console.log('Error during download. Code = ' + error.code);
              resolved = false;
            }
          );
        });

////        console.log('Has FileTransfer');
//        var fileTransfer = new FileTransfer(),
//          file = storage + fileName;
////        console.log(storage);
//        // LOG console.log('About to start transfer');
//        fileTransfer.download(assetURL, file,
//          function (entry) {
//              // LOG console.log('Success!');
////            console.log('Success ' + entry.fullPath);
////            return entry.fullPath;
//  //          app.start();
//            fullpath = entry;
//            resolved = true;
//          },
//          function (err) {
////            console.log('Error ' + assetURL + ' ' + fileName);
//            resolved = false;
////            return placeholder_img;
//  //          console.dir(err);
//          });
      } else {
//        console.log('Not FileTransfer');
//        return placeholder_img;
        resolved = false;
      }
    },
    // I'm only called when the file exists or has been downloaded.
    start: function () {
      'use strict';
      // LOG console.log('App ready!');
    },
    resolved: function () {
      'use strict';
//      console.log('resolved ' + resolved);
      resolved = true;
//      return file;
    },
    uploadImage: function (src) {
      'use strict';
      var dir = home_url + '/wp-content/uploads/',//decodeURI()
        // URL of our asset
        assetURL = dir + src,
        fileName = extractUploadFile(assetURL),
        file,
        image;
      resolved = false;
      image = placeholder_img;
//      console.log('placeholder_img ' + placeholder_img);
      file = storage + fileName;
//      console.log(typeof cordova !== 'undefined');
      if (typeof cordova !== 'undefined') {
//        console.log('cordova.file defined');
        // File name of our important data file we didn't ship with the app
        window.resolveLocalFileSystemURL(file, app.resolved, app.downloadAsset(assetURL, fileName));
//        console.log(resolved);
        if (resolved) {
          image = placeholder_img;
        } else {
//          console.log('fullpath ' + fullpath);
          image = fileName;
        }
      } else {
//        console.log('cordova.file undefined');
        image = placeholder_img;
      }
      resolved = false;
      image = placeholder_img;
      //console.log('image ' + image);
      return image;
    },
//    onFSSuccess: function (fs) {
//      'use strict';
//      console.log('FSSuccess');
//      fileSystem = fs;
//
//      console.log( 'Got the file system: ' + fileSystem.name + "\n" +
//      'root entry name is ' + fileSystem.root.name)
//    },
//    onError: function () {
//      'use strict';
//      console.log('onError');
//    },
    init: function () {
      'use strict';

//      storage = cordova.file.dataDirectory;
      app.switchConnectState();
      app.cacheData();
//      window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
//          // create the download directory is doesn't exist
//        fs.root.getDirectory('br.com.moveisjae.app', { create: true });
//        storage = fs.root.toURL() + 'br.com.moveisjae.app/'
//        console.log(storage);
//      });
////      storage = cordova.file.dataDirectory;
//      $('body').append(storage);
      // request the persistent file system
//      window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, app.onFSSuccess, app.onError);
//      storage = filesystem.root.fullPath;
    },
    // Update DOM on a Received Event
    receivedEvent: function (id) {
      'use strict';
      var body = document.getElementsByTagName('body')[0];
      body.classList.remove('listening');
    },
    switchConnectState: function () {
      'use strict';
      if (navigator.onLine) {
        $('body').addClass('online');
        $('body').removeClass('offline');
        // LOG console.log('Device is online');
      } else {
        $('body').addClass('offline');
        $('body').removeClass('online');
        // LOG console.log('Device is offline');
      }
    },
    fireEvent: function (name, data) {
      'use strict';
      var e = document.createEvent('Event');
      e.initEvent(name, true, true);
      e.data = data;
      window.dispatchEvent(e);
    },
    fetch: function (url, callback) {
      'use strict';
      callback = typeof callback !== 'undefined' ? callback : app.dataLoaded;
      var xhr = new XMLHttpRequest(),

        maxWaitTime = 4000,

        noResponseTimer = setTimeout(function () {
          xhr.abort();
          app.fireEvent('connectiontimeout', {});
          if (!!localStorage[url]) {
            // We have some data cached, return that to the callback.
            callback(localStorage[url]);
            return;
          }
        }, maxWaitTime);

      xhr.onreadystatechange = function (e) {

        if (xhr.readyState !== 4) {
          return;
        }

        if (xhr.status === 200) {
          app.fireEvent('goodconnection', {});
          clearTimeout(noResponseTimer);
          // Save the data to local storage
          localStorage[url] = xhr.responseText;
          // call the handler
          callback(xhr.responseText);
        } else {
          app.fireEvent('connectionerror', {});
          // There is an error of some kind, use our cached copy (if available).
          if (!!localStorage[url]) {
            // We have some data cached, return that to the callback.
            callback(localStorage[url]);
            return;
          }
        }
      };
      xhr.open('GET', url);
      xhr.send();
    },
    cacheData: function () {
      'use strict';
      app.fetch('http://www.moveisjae.com.br/offline', app.dataLoaded);
    },
    dataLoaded: function (json) {
      'use strict';
      // LOG console.log('Data loaded');
      $('body').removeClass('loading');
      localStorage.setItem('data', json);
      app.appParams();
      app.retrieveProductCategories();
      app.menuProductCats();
      app.productSlugs();
      app.downloadThumbs();
    },
    productSlugs: function () {
      'use strict';
      var data = app.getData(),
        products = {};
      $.each(data.products, function (slug, item) {
        products[item.id] = slug;
      });
      localStorage.setItem('products', JSON.stringify(products));
      // LOG console.log('Record product slugs');
    },
    getData: function () {
      'use strict';
      var data = localStorage.getItem('data');
      data = JSON.parse(data);
      return data;
    },
    appParams: function () {
      'use strict';
      var data = app.getData();
      home_url = data.home_url;
      permastruct = data.permastruct;
      // LOG console.log('Finish set params');
    },
    retrieveProductCategories: function () {
      'use strict';
      var data,
        range = {};

      data = app.getData();

      range.top = [];
      range.mdp = [];

      $.each(data.categories, function (slug, term) {
        delete term.products;
        term.slug = slug;
        if (term.taxonomy === 'linhamdp') {
          range.mdp.push(term);
        } else {
          range.top.push(term);
        }
      });
      localStorage.setItem('categories', JSON.stringify(range));
      // LOG console.log('Retrieve cetegories');
    },
    submenuProductCats: function (menuid, items) {
      'use strict';
      var submenu = $('#menu-item-' + menuid + ' .sub-menu'),
        taxonomy,
        itemid,
        menuitem,
        href;
      $.each(items, function (menuorder, term) {
        itemid = 'menu-item-' + term.id;
        menuitem = $('<li>').attr('id', itemid).addClass(itemid);
        taxonomy = term.taxonomy;
        href = permastruct[taxonomy];
        href = href.replace('%%' + taxonomy + '%%', term.slug) + '/';
        menuitem.prepend($('<a>').attr('href', href).text(term.name));
        submenu.append(menuitem);
      });
    },
    menuProductCats: function () {
      'use strict';
      var categories = JSON.parse(localStorage.getItem('categories'));
      app.submenuProductCats(298, categories.top.filter(isNotNull));
      app.submenuProductCats(1181, categories.mdp.filter(isNotNull));
      app.menuItemProductCatsClick();
      $('#menu-item-173, #menu-item-1193, #menu-item-172').click(function (e) {
        e.preventDefault();
      });
    },
    switchProductCat: function (slug) {
      'use strict';
      var data = app.getData(),
        category = data.categories[slug],
        product,
        href,
        image,
        title,
        productitem,
        src;
      $('body').removeClass('home');
      $('body').addClass('product-cat');
      $('.navbar-collapse').collapse('toggle');
      $('.tax-product_cat .page-title').text(category.name);
      $('.tax-product_cat .term-description p').text(category.description);
      $('.tax-product_cat .products').html('');
      $.each(category.products, function (i, id) {
        slug = productSlug(id);
        product = data.products[slug];
        productitem = $('<div>').addClass('col-sm-4 post-' + id + ' product type-product produto type-produto');
        href = 'produto/' + slug + '/';
        image = $('<img>');
        image.attr('width', '312');
        image.attr('height', '281');
        src = app.uploadImage(product.featured_src);
//        console.log('SRC: ' + src);
        image.attr('src', src);
        image.attr('alt', product.title);
        title = $('<h2>');
        title.html($('<a>').text(product.title));
        productitem.prepend($('<a>').attr('href', href).html(image));
        productitem.append(title);
        $('.tax-product_cat .products').append(productitem);
        $('.tax-product_cat a').click(function (e) {
          e.preventDefault();
        });
      });
      // LOG console.log('Populate sub-menus categories');
    },
    menuItemProductCatsClick: function () {
      'use strict';
      var slug;
      $('#menu-item-298, #menu-item-1181').each(function () {
        $(this).find('.sub-menu a').each(function () {
          $(this).click(function (e) {
            e.preventDefault();
            slug = $(this).attr('href');
            slug = extractSlug(slug).replace('/', '');
            app.switchProductCat(slug);
          });
        });
      });
    },
    downloadThumbs: function () {
      'use strict';
      var data = app.getData();
      console.log('Download Thumbs');
      $.each(data.products, function (slug, product) {
//        console.log(app.uploadImage(product.featured_src));
//        console.log('product.featured_src ' + product.featured_src);
        app.uploadImage(product.featured_src);
      });
//      app.uploadImage('2015/04/Asturia-80-C-Pes-Vicenza-Amarelo-Retro-Gavetao-312x281.jpg');

//      console.log(cordova.file.dataDirectory);
//      if (typeof LocalFileSystem !== 'undefined') {
//        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0,
//          function onFileSystemSuccess(fileSystem) {
//            console.log('Persistent');
//            fileSystem.root.getFile(
//              'dummy.html',
//              {create: true, exclusive: false},
//              function gotFileEntry(fileEntry) {
//                var sPath = fileEntry.fullPath.replace('dummy.html', ''),
//                  fileTransfer = new FileTransfer();
//                console.log(sPath);
//                fileEntry.remove();
//
//                fileTransfer.download(
//                  'http://www.moveisjae.com.br/wp-content/uploads/2015/04/Asturia-80-C-Pes-Vicenza-Amarelo-Retro-Gavetao-550x495.jpg',
//                  sPath + "thumb.jpg",
//                  function (theFile) {
//                    console.log("download complete: " + theFile.toURI());
//                  },
//                  function (error) {
//                    console.log("download error source " + error.source);
//                    console.log("download error target " + error.target);
//                    console.log("upload error code: " + error.code);
//                  }
//                );
//              }
//            );
//          }, fail);
//      }
    }
  };
